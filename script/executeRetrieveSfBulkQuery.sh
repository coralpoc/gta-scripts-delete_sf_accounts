#!/bin/bash
# requires authenticated force.exe install, will run as logged in credentials

QUERY_FILE=$1
S_OBJECT=$2

INST_ID=${QUERY_FILE%.*}
THEPID=$BASHPID 
TEMP_DIR=/tmp/$INST_ID.$THEPID

if [ $# -ne 2 ]
then
  echo "Takes a SOQL query from a text file, creates a job on SF
         to execute it, waits for completion and downloads result"
  echo "\n"
  echo "Usage: $0 query.file S_OBJECT"
  echo "    e.g. $0 queryAccounts.soql Accounts"
  exit 1
fi

mkdir $TEMP_DIR

trap erroredExit 1 2 3 6

erroredExit()
{
    echo "Script did not complete successfully"
    echo "tmp files available for debug in \"$TEMP_DIR\""¦
    exit 1
}

function endScript {
    echo "Processing complete"
    echo "Cleaning tmp folder..."
    rm -r $TEMP_DIR
    echo "Done cleanup ... quitting."
    exit 0
}

force bulk query $S_OBJECT "$(cat $QUERY_FILE)" > $TEMP_DIR/addBulkJob.out

grep "force bulk query status" $TEMP_DIR/addBulkJob.out > $TEMP_DIR/CheckJobCommand.txt
grep "force bulk query retrieve" $TEMP_DIR/addBulkJob.out > $TEMP_DIR/GetDataCommand.txt

ISNOTCOMPLETED=99
while [ $ISNOTCOMPLETED -ne 0 ]
do
echo Checking results of \"$S_OBJECT.$INST_ID\"
eval $(cat $TEMP_DIR/CheckJobCommand.txt) | tee $TEMP_DIR/CheckJobCommand.result.txt
grep --quiet Completed $TEMP_DIR/CheckJobCommand.result.txt
ISNOTCOMPLETED=$?
grep --quiet Failed $TEMP_DIR/CheckJobCommand.result.txt
ISNOTFAILED=$?
if [ $ISNOTFAILED -eq 1 ]; then 
    echo "Waiting for results..."
    sleep 5
    else
    echo "Batch job failed..."
    erroredExit    
fi 

done

eval $(cat $TEMP_DIR/GetDataCommand.txt) > $S_OBJECT.$INST_ID.csv
echo "Success: Query output to $S_OBJECT.$INST_ID.csv"

endScript