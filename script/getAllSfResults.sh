#!/bin/bash

declare -a procs=("./script/executeRetrieveSfBulkQuery.sh Account.RetailActive.soql Account"
    "./script/executeRetrieveSfBulkQuery.sh Case.All.soql Case"
    "./script/executeRetrieveSfBulkQuery.sh Account.1100dayActive.req.soql Account"
    "./script/executeRetrieveSfBulkQuery.sh Account.All.soql Account")

# run processes and store pids in array
cnt=0
for i in "${procs[@]}"; do
    #echo $cnt \n
    #echo $i
    $i &
    pids[$cnt]=$!
    ((cnt++))
done

## wait for all pids
for pid in ${pids[*]}; do
    wait $pid
done