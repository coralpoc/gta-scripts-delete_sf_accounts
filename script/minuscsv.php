<?php
include('util.php');

if (isset($argv[5])){
    $sourceFile         = $argv[1];
    $sourceColumnNum    = $argv[2];
    $removeKeyFile      = $argv[3];
    $removeColumnNum    = $argv[4];
    $outFile            = $argv[5];
} else {
    file_put_contents("php://stderr", "Must supply 2 input files each followed by the column containing the key to match\r\n
    Usage: \r\n
        $argv[0] filenameToTrim.csv n fileNameToRemove.csv n outputfile.csv");
        die(1);
}

$startInstant = (new DateTime())->format('U');

//attempt read all removal keys in to array
$cntLine = 0;
$removeKeys = Array();
$removeLines = file($removeKeyFile);
foreach ($removeLines as $line){
    $cntLine++;
    if($cntLine ==1){
        $keyField = explode(",",trim($line))[$removeColumnNum-1];
        loggy ( "Reading the following as the CASE key " . $keyField);
    } else {
        $removeKeys[explode(",",trim($line))[$removeColumnNum-1]] = null;
    }

}
loggy("Got all the records to exclude. " . sizeof($removeKeys));

$handle = fopen($sourceFile, "r");
$header = fgets($handle);
$output = '';
$removed = '';
$lineNumber =0;
loggy ( "Writing header..." );
file_put_contents($outFile,$header);

while (($line = fgets($handle)) !== false) {
    $lineNumber++;
    if($lineNumber % 1000 == 0){
        loggy ( $lineNumber);
    }
    $keyField = array_map('trim', explode(",",$line))[$sourceColumnNum -1];

    if(//true ||
    array_key_exists($keyField, $removeKeys)){
        $removed .= $line;
        //loggy ("This key _IS_ in the second list. " . sizeof($removed));
    } else {
        $output .= $line;
    }

    if($lineNumber % 100000 == 0
//    || TRUE
    || memory_get_usage() > (return_bytes(ini_get('memory_limit')) * "0.4"))
    {        
        loggy ( "Writing file data..." );
        file_put_contents($outFile,$output, FILE_APPEND);
        loggy ("Wrote: ".strlen($output) . " bytes to ".$outFile.PHP_EOL);
        file_put_contents($outFile.".removed",$removed, FILE_APPEND);
        loggy ("Wrote: ".strlen($removed) . " bytes to ".$outFile.".removed".PHP_EOL);
        //  clear output
        $output = null;
        $removed = null;
        
    };
}
//save leftover ouput
file_put_contents($outFile,$output, FILE_APPEND);
file_put_contents($outFile.".removed",$removed, FILE_APPEND);
fclose($handle);

$endInstant = (new DateTime())->format('U');
$runtime = $endInstant - $startInstant;
loggy( " Complete. Lines: ".$lineNumber.PHP_EOL);
loggy( " Runtime: ". $runtime . " seconds. Rate: ". $lineNumber / $runtime.PHP_EOL);
