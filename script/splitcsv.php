<?php
include('util.php');

if (isset($argv[1])){
    $inputFile = $argv[1];
} else {
    file_put_contents("php://stderr", "Must supply input file.\r\n
    Usage: \r\n
        $argv[0] filename.csv [lines per output file]");
        die(1);
}

if (isset($argv[2])){
    $chunkSize = $argv[2];
} else {
    $chunkSize = 2000000;
}

file_put_contents("php://stdout", (new DateTime())->format('c')." " . "Begin".PHP_EOL);
$startInstant = (new DateTime())->format('U');

$handle = fopen($inputFile, "r");
$header = fgets($handle);
$output = '';
$lineNumber =0;
$fileNumber =0;

while (($line = fgets($handle)) !== false) {
    //  append to intended output 
    $output .= $line;

    $lineNumber++;
    if($lineNumber % $chunkSize == 0 
            //Check we're not going to blow memory. 
            //file_put_contents breifly doubles memory usage.
            || memory_get_usage() > (return_bytes(ini_get('memory_limit')) * "0.4"))
            {        
        $fileNumber++;
        $formattedFileNumber = str_pad($fileNumber, 3, '0', STR_PAD_LEFT);
        $formattedFileName = $inputFile."_".$formattedFileNumber.".csv";
        file_put_contents("php://stdout", (new DateTime())->format('c')." " . "Writing header...".PHP_EOL);
        file_put_contents($formattedFileName,$header);
        file_put_contents("php://stdout", (new DateTime())->format('c')." " . "Writing file data...".PHP_EOL);
        file_put_contents($formattedFileName,$output, FILE_APPEND);
        file_put_contents("php://stdout", (new DateTime())->format('c')." " . "Wrote: ".strlen($output) . " bytes to ".$formattedFileName.PHP_EOL);
//  clear output once written 
        $output = null;
    }
}
//save leftover ouput
file_put_contents($inputFile."_".$formattedFileNumber.".csv",$output, FILE_APPEND);
fclose($handle);

$endInstant = (new DateTime())->format('U');
$runtime = $endInstant - $startInstant;
file_put_contents("php://stdout", (new DateTime())->format('c')." " . " Complete. Files: ".$fileNumber." Lines: ".$lineNumber.PHP_EOL);
file_put_contents("php://stdout", (new DateTime())->format('c')." " . " Runtime: ". $runtime . " seconds. Rate: ". $lineNumber / $runtime.PHP_EOL);
