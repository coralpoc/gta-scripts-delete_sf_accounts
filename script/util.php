<?php

function loggy ($message = "", $isErr = false){
    
    $dateStamp = (new DateTime())->format('c')." " ;

    switch ($isErr) {
        case false:
            file_put_contents("php://stdout",$dateStamp . "INFO: ". $message .PHP_EOL);
            break;
        
        default:
            file_put_contents("php://stderr",$dateStamp . "ERROR: ". $message .PHP_EOL);
            break;
    }
}

function return_bytes($val) {
    $val = trim($val);
    $last = strtolower($val[strlen($val)-1]);
    switch($last) {
        // The 'G' modifier is available since PHP 5.1.0
        case 'g':
            $val *= 1024;
        case 'm':
            $val *= 1024;
        case 'k':
            $val *= 1024;
    }

    return $val;
}